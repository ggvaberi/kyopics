package libs

import (
	"net/http"

	"golang.org/x/net/html"
)

func Search(url string) string {
	LogDebug("Start search for " + url)

	LogDebug("Start search for prompt image")

	imgurl := getPromptImage(url)

	if imgurl == "" {
		LogError("Cannot find prompt image for: " + url)

		return ""
	} else {
		return imgurl
	}

	LogDebug("Prompt image is: " + imgurl)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")

	res, err := client.Do(req)

	//res, err := client.Get("https://www.google.com/search?q=" + s)

	if err != nil || res == nil {
		LogError("Search " + url + " failed.")
		return ""
	}

	defer res.Body.Close()

	tok := html.NewTokenizer(res.Body)

	LogDebug("Start search procedure...")

search_loop:
	for {
		stat := tok.Next()

		switch {
		case stat == html.ErrorToken:
			LogDebug("Tokenizer error.")
			break search_loop
		case stat == html.StartTagToken:
			tn := tok.Token()
			LogDebug("StartTagToken " + tn.Data)
		case stat == html.EndTagToken:
			tn := tok.Token()
			LogDebug("EndTagToken " + tn.Data)
		case stat == html.SelfClosingTagToken:
			tn := tok.Token()
			LogDebug("SelfClosingTagToken " + tn.Data)
		}
	}

	return ""
}

func getPromptImage(url string) string {
	result := ""

	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")

	res, err := client.Do(req)

	//res, err := client.Get("https://www.google.com/search?q=" + s)

	if err != nil || res == nil {
		LogError("Retrieve prompt image for " + url + " failed.")
		return ""
	}

	defer res.Body.Close()

	tok := html.NewTokenizer(res.Body)

	LogDebug("Start search procedure...")

search_loop:
	for {
		stat := tok.Next()

		switch {
		case stat == html.ErrorToken:
			LogDebug("Tokenizer error.")
			break search_loop
		case stat == html.StartTagToken:
			tn := tok.Token()
			LogDebug("StartTagToken " + tn.Data)

			h := getImageFromLink(&tn)

			if h != "" {
				return h
			}
		case stat == html.EndTagToken:
			tn := tok.Token()
			LogDebug("EndTagToken " + tn.Data)
		case stat == html.SelfClosingTagToken:
			tn := tok.Token()
			LogDebug("SelfClosingTagToken " + tn.Data)

			if tn.Data == "link" {
				h := getImageFromLink(&tn)

				if h != "" {
					return h
				}
				/*a := GetTokenAttr(&tn, "link", "rel")

				if a != "" && a == "image_src" {
					h := GetTokenAttr(&tn, "link", "href")

				}*/
			}
		}
	}

	return result
}

func getImageFromLink(tk *html.Token) string {
	if tk.Data == "link" {
		a := GetTokenAttr(tk, "link", "rel")

		if a != "" && a == "image_src" {
			h := GetTokenAttr(tk, "link", "href")

			if h != "" {
				return h
			}
		}
	}

	return ""
}
