package libs

import (
	"os"
	//"log"
)

var loglevel uint = 5

//SetLogLevel ...
func SetLogLevel(lvl uint) {
	loglevel = lvl
}

func print(lvl uint, txt string) {
	if lvl > loglevel {
		return
	}

	tex_level := ""

	switch lvl {
	case 0:
		tex_level = "ERROR"
		break
	case 1:
		tex_level = "WARNING"
		break
	case 2:
		tex_level = "INFO"
		break
	case 3:
		tex_level = "DEBUG"
		break
	default:
		tex_level = "GENERAL"
	}

	os.Stderr.WriteString(tex_level + ": " + txt + "\n")
}

//LogError ...
func LogError(txt string) {
	print(0, txt)
}

//LogWarning ...
func LogWarning(txt string) {
	print(1, txt)
}

//LogInfo ...
func LogInfo(txt string) {
	print(2, txt)
}

//LogDebug ...
func LogDebug(txt string) {
	print(3, txt)
}

//LogGeneral ...
func LogGeneral(txt string) {
	print(4, txt)
}
