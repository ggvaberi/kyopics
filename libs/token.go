package libs

import "golang.org/x/net/html"

//IsToken ...
func IsToken(tok *html.Token, tag string, class string) bool {
	if tok == nil {
		return false
	}

	if tok.Data != tag {
		return false
	}

	for _, a := range tok.Attr {
		if a.Key == "class" && a.Val == class {
			return true
		}
	}

	return false
}

//IsTokenAttr ...
func IsTokenAttr(tok *html.Token, tag string, key string, val string) bool {
	if tok == nil {
		return false
	}

	if tok.Data != tag {
		return false
	}

	for _, a := range tok.Attr {
		if a.Key == key && a.Val == val {
			return true
		}
	}

	return false
}

//GetTokenAttr ...
func GetTokenAttr(tok *html.Token, tag string, key string) string {
	if tok == nil {
		return ""
	}

	if tok.Data != tag {
		return ""
	}

	for _, a := range tok.Attr {
		if a.Key == key {
			return a.Val
		}
	}

	return ""
}
