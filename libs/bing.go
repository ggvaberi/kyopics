package libs

import (
	"strconv"
	"net/http"

	"golang.org/x/net/html"
)


func SearchBingImg(url string) string {
	result := ""
	request := "https://www.bing.com/images/search?q=imgurl:" + url + "&view=detailv2&iss=sbi&form=IRSBIQ&selectedindex=0"
	client := &http.Client{}
	req, _ := http.NewRequest("GET", request, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")

	res, err := client.Do(req)

	//res, err := client.Get("https://www.google.com/search?q=" + s)

	if err != nil || res == nil {
		LogError("Retrieve prompt image for " + url + " failed.")
		return ""
	}

	defer res.Body.Close()

	var b []byte

	n, err := res.Body.Read(b)

	if err != nil || n <= 0 {
		LogDebug("bing image search error: " + strconv.Itoa(n))

		if err != nil {
			LogDebug("bing image search error is " + err.Error())
		}
	}

	var s string

	s = string(b[:])

	LogDebug("bing image search: " + s)

	if s != "" {
		return ""
	}

	tok := html.NewTokenizer(res.Body)

	LogDebug("Start search procedure...")

search_loop:
	for {
		stat := tok.Next()

		switch {
		case stat == html.ErrorToken:
			LogDebug("Tokenizer error.")
			break search_loop
		case stat == html.StartTagToken:
			tn := tok.Token()
			LogDebug("StartTagToken " + tn.Data)

			h := getImageFromLink(&tn)

			if h != "" {
				return h
			}
		case stat == html.EndTagToken:
			tn := tok.Token()
			LogDebug("EndTagToken " + tn.Data)
		case stat == html.SelfClosingTagToken:
			tn := tok.Token()
			LogDebug("SelfClosingTagToken " + tn.Data)

			if tn.Data == "link" {
				h := getImageFromLink(&tn)

				if h != "" {
					return h
				}
				/*a := GetTokenAttr(&tn, "link", "rel")

				if a != "" && a == "image_src" {
					h := GetTokenAttr(&tn, "link", "href")

				}*/
			}
		}
	}

	return result
}
