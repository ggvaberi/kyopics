package main

import (
	"io"
	"kyopics/libs"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"gitlab.com/ggvaberi/kjinmod"
)

//import "encoding/json"


//FnRoute is ...
type FnRoute func(http.ResponseWriter, *http.Request)

//WebRoute is ...
type WebRoute struct {
	id     string
	handle FnRoute
}

//WebHandler is ...
type WebHandler struct {
	mu sync.Mutex // guards n
	n  int

	routes []*WebRoute
}

//Add is ...
func (h *WebHandler) Add(id string, handle FnRoute) {
	var p = new(WebRoute)

	p.id = id
	p.handle = handle

	h.routes = append(h.routes, p)
}

func getValue(r *http.Request, key string) string {
	keys, ok := r.URL.Query()[key]

	if !ok || len(keys[0]) < 1 {
		libs.LogDebug("Url query key " + key + " is missing.")

		return ""
	}

	return keys[0]
}

func (h *WebHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var url = r.URL.Path

	libs.LogDebug("Serve url " + url)

	if len(url) > len("/static/") && url[0:8] == "/static/" {
		handleStatic(w, r)
	} else {
		libs.LogDebug("routes count " + strconv.Itoa(len(h.routes)))

		for _, rt := range h.routes {
			if rt.id == url {
				rt.handle(w, r)
			}
		}
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	//t := libs.NewPage()
	//t.Init("index.html")

	t := kjinmod.New()
	t.Init("index.html", "static/html")

	io.WriteString(w, t.Content)
}

func handleSearch(w http.ResponseWriter, r *http.Request) {
	url := getValue(r, "url")

	libs.LogDebug("Handler search will search for: " + url)

	h := libs.Search(url)

	if h != "" {
		libs.LogDebug("Starting search image by BING")
		libs.SearchBingImg(h)
	}

	t := kjinmod.New()

	t.Init("search.html", "static/html")

	t.Content = strings.ReplaceAll(t.Content, "{%IMG_URL%}", h)

	io.WriteString(w, t.Content)
}

func handleStatic(w http.ResponseWriter, r *http.Request) {
	t := libs.NewPage()

	t.Init("index.html")

	io.WriteString(w, t.Content)
}

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "5000"
	}

	libs.SetLogLevel(5)

	libs.LogInfo("Using port: " + port)

	var h = new(WebHandler)

	h.Add("/search", handleSearch)
	h.Add("/", handleIndex)

	//h.Add("/sitemap.xml", handleSitemap)
	//h.Add("/BingSiteAuth.xml", handleBingSiteAuth)
	//h.Add("/googleb295dd6d4113b434.html", handleGoogleSiteAuth)

	http.Handle("/", h)
	http.ListenAndServe(":"+port, nil)
}
