package kjinmod

import (
	"log"
        "io/ioutil"
)

type Page struct {
        Content string
}

func New() *Page {
        page := &Page{Content: ""}

        return page
}

func (self *Page) Init(tem string, dir string) {
        log.Print("kjinmod init page " + tem)

        content, err := ioutil.ReadFile(dir + "/" + tem)

        if err != nil {
                log.Print("kjinmod cannot read file " + tem)
        }

        txt := string(content)

        self.Content = string(txt)
}

func (seld *Page) Compile() {
}
